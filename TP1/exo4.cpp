#include <iostream>
#include <string>

int maxValue(int *tab, int size) {
  int maxVal = tab[0];
  for (int i = 1; i < size; i++) {
    if ( tab[i] > maxVal ) maxVal = tab[i];
  }
  return maxVal;
}

int main() {
  int size;
  std::cin >> size;
  std::cout << std::endl;

  //int *tab = new (sizeof(int) * size) int*;

  int *tab = new int[size];

  for ( int i=0; i<size; i++) {
    std::cout << "tab[" << i << "] :";
    std::cin >> tab[i];
    std::cout << std::endl;
  }

  std::cout << &tab[0] << &tab[1] << &tab[2] << std::endl;
  std::cout << sizeof(tab[0]) << std::endl;
  std::cout << *tab << std::endl;
  std::cout << *tab+4 << std::endl;


  std::cout << "max : " << maxValue(tab, size) << std::endl;
  delete[] tab;
  return 0;
}

