// ================================
// POO C++ - IMAC 2
// TP 1 - Exercice 6
// ================================

#include<iostream>
#include<vector>

#include "chrono.hpp"

namespace TP_CPP_IMAC2
{
  float mean(const std::vector<int> &vec)
  {
    int mean = 0;
    for ( int i = 0; i < int( vec.size() ); ++i ) {
      mean += vec[i];
    }

    return mean / vec.size();
  }

  float meanCopy(const std::vector<int> vec)
  {
    int mean = 0;
    for ( int i = 0; i < int( vec.size() ); ++i ) {
      mean += vec[i];
    }

    return mean / vec.size();

  }

  int main()
  {
    std::vector<int> integer;
    int size;
    std::cout << "taille : ";
    std::cin >> size;
    std::cout << std::endl;

    for (int i=0; i<size; i++) {
      integer.push_back(i%10);
    }

    std::cout << "taille = " << integer.size() << std::endl;

    float vec_mean;
    Chrono chrono;
    chrono.start();
    vec_mean = mean(integer);
    chrono.stop();

    std::cout << "mean = " << vec_mean << ", time spent = "<< chrono.timeSpan() << std::endl;

    chrono.start();
    vec_mean = meanCopy(integer);
    chrono.stop();

    std::cout << "meanCopy = " << vec_mean << ", time spent = "<< chrono.timeSpan() << std::endl;

    return 0;
  }
}

// Fonction main classique, point d'entr�e du programme
int main()
{
  // Appel de la fonction main de l'espace de nom TP_CPP_IMAC2
  return TP_CPP_IMAC2::main();
}
