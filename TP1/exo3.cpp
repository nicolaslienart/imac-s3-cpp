#include <iostream>
#include <string>

int main() {
  std::string str;
  std::cout << "string stp : ";
  std::cin >> str;
  std::cout << std::endl;
  std::cout << "Size : " << str.size() << std::endl << "Last elm : " << str[str.size() - 1] << std::endl;

  str.erase(str.end()-1);

  std::cout << "Size : " << str.size() << std::endl << "Last elm : " << str[str.size() - 1] << std::endl; // std::string::popback()

  str.append("IMAC");//insert ou bien l'opérateur "+" std::string::insert
  std::cout << str << std::endl;
  return 0;
}
