// ================================
// POO C++ - IMAC 2
// TP 1 - Exercice 5
// ================================

#include<iostream>
#include<vector>

namespace TP_CPP_IMAC2
{
  int main()
  {
    return 0;
  }
}

// Fonction main classique, point d'entr�e du programme
int main(int argc, char *argv[])
{
  // Appel de la fonction main de l'espace de nom TP_CPP_IMAC2
  std::vector<int> integer;
  int size;
  std::cout << "taille : ";
  std::cin >> size;
  std::cout << std::endl;

  for (int i=0; i<size; i++) {
    integer.push_back(i%10);
  }

  std::cout << "taille = " << integer.size() << std::endl;
  return TP_CPP_IMAC2::main(argc, argv);
}


