#include "VectorD.hpp"

VectorD::VectorD(size_t size)
  : m_size(size)
{
  m_data = new double[size];
}

VectorD VectorD::loadFromFile(const std::string& fileName)
{
  std::ifstream vectorFile;
  vectorFile.open(fileName, std::ios::binary | std::ios::in);
  if ( vectorFile.is_open() == false ) {
    std::cout << "Couldn't open file:" << fileName << std::endl;
    return EXIT_FAILURE;
  }

  size_t size;
  vectorFile >> size;
  VectorD vec(size);

  for ( size_t i = 0; i < size; i++ ) {
    double value;
    vectorFile >> value;
    vec[i] = value;
  }

  vectorFile.close();

  return vec;
}

VectorD::~VectorD()
{
  delete[] m_data;
}

VectorD::VectorD(const VectorD &vec)
  : VectorD(vec.m_size)
{
  std::copy(vec.m_data, vec.m_data + vec.m_size, m_data);
}

VectorD::VectorD(const size_t size, const double value)
  : VectorD(size)
{ 
  for ( size_t i = 0; i < size; i++ ) {
    m_data[i] = value;
  }
}


double VectorD::length() const
{
  double res = 0;
  for ( size_t i = 0; i < m_size; i++) {
    res += std::pow( this->m_data[i], 2 );
  }
  return std::sqrt(res);
}

static double length(const VectorD &vec)
{
  double res = 0;
  for ( size_t i = 0; i < vec.size(); ++i ) {
    res += std::pow( vec[i], 2 );
  }
  return std::sqrt(res);
}
void VectorD::scalar(const double scalar)
{
  for ( size_t i = 0; i < m_size; i++) {
    m_data[i] *= scalar;
  }
}

void VectorD::operator*(const double scalar)
{
  this->scalar(scalar);
}

void VectorD::normalize()
{
  double length = this->length();
  this->scalar(1.0 / length);
}


// Getters
inline size_t VectorD::size() const
{
  return this->m_size;
}

const double& VectorD::operator[](const size_t id) const
{
  return m_data[id];
}

double& VectorD::operator[](const size_t id)
{
  return m_data[id];
}


// Setters
void VectorD::set(const size_t i, const double value)
{
  m_data[i] = value;
}


// Static functions
VectorD VectorD::VectorDRandom(const size_t size)
{
  double minValue = std::numeric_limits<double>::min();
  double maxValue = std::numeric_limits<double>::max();

  VectorD vec(size);

  std::random_device random; // obtain a random number from hardware
  std::mt19937 engine(random()); // seed the generator
  std::uniform_real_distribution<double> distribution(minValue, maxValue); // define the range

  for ( size_t i = 0; i < size; ++i ) {
    vec[i] = distribution(engine);
  }

  return vec;
}

VectorD VectorD::VectorDRandom(const size_t size, const double minValue, const double maxValue)
{
  VectorD vec(size);

  std::random_device random; // obtain a random number from hardware
  std::mt19937 engine(random()); // seed the generator
  std::uniform_real_distribution<double> distribution(minValue, maxValue); // define the range

  for ( size_t i = 0; i < size; ++i ) {
    vec[i] = distribution(engine);
  }

  return vec;

}

std::ostream& operator<<(std::ostream& stream, const VectorD &vec)
{
  for ( size_t i = 0; i < vec.size() -1; i++ ) {
    stream << vec[i] << " ";
  }
  stream << vec[vec.size() -1];

  return stream;
}

void VectorD::print(const VectorD& vec)
{
  std::cout << vec << std::endl;
}

void VectorD::print() const
{
  std::cout << *this << std::endl;
}

int VectorD::writeToFile(const std::string& fileName) const
{
  std::ofstream vectorFile;
  vectorFile.open(fileName, std::ios::out | std::ios::binary);

  if ( vectorFile.is_open() == false ) {
    std::cout << "Couldn't open file:" << fileName << std::endl;
    return EXIT_FAILURE;
  }

  vectorFile << m_size << std::endl;
  for ( size_t i = 0; i < m_size; i++ ) {
    vectorFile << m_data[i] << " ";
  }

  vectorFile.close();

  return EXIT_SUCCESS;
}
