#ifndef __VECTOR_D_
#define __VECTOR_D_

#include <cstdlib>
#include <cmath>
#include <limits>
#include <algorithm>
#include <random>
#include <iostream>
#include <string>
#include <fstream>

class VectorD {
  private:
    size_t m_size;
    double *m_data;

  public:
    // Constructors
    VectorD(size_t size);
    VectorD(const std::string &fileName);
    VectorD(const VectorD &vec);
    VectorD(const size_t size, const double value);
    ~VectorD();

    static VectorD VectorDRandom(const size_t size);
    static VectorD VectorDRandom(const size_t size, const double minValue, const double maxValue);

    // Getters
    inline size_t size() const;
    const double& operator[](const size_t id) const;
    double& operator[](const size_t id);

    // Setters
    void set(const size_t i, const double value);

    // Other stuff
    void operator*(const double scalar);
    static double length(const VectorD &vec);
    double length() const;
    void normalize();
    void scalar(const double scalar);

    // OUTPUT
    void print() const;
    static void print(const VectorD& vec);

    int writeToFile(const std::string& fileName) const;
    static VectorD loadFromFile(const std::string& fileName);

};

/* This function is out of the class because it is not called by VectorD.
 * It's called by a cout ot cerr, witch is a stream, and it takes as an argmument the vector
 */

std::ostream& operator<<(std::ostream& stream, const VectorD &vec);

#endif /* ifndef __VECTOR_D_ */
