#include "VectorD.hpp"
#include <iostream>

int main( void )
{
  VectorD a = VectorD::loadFromFile("vector_a");
  //VectorD a = VectorD::VectorDRandom(10);
  //VectorD b(5, 1.0);
  //VectorD b = VectorD::VectorDRandom(10, 1.0, 2.0);
  //a.set(1, 12.65225);
  //b.set(b.size() -1, 12.65225);

  //b.print();
  //VectorD::print(a);
  //b.normalize();

  std::cout << a << std::endl;
  //std::cout << b << std::endl;

  //a.writeToFile("vector_a");

  return 0;
}
