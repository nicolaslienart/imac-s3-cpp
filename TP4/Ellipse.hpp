#ifndef __ELLIPSE_
#define __ELLIPSE_

#define M_PI 3.14

class Ellipse {
  protected:
    double m_petit_rayon_a;
    double m_grand_rayon_b;
  public:
    Ellipse() = default;
    ~Ellipse() = default;
    Ellipse(const double petit_rayon_a, const double grand_rayon_b);

    double surface();

    double& petitRayon();
    double& grandRayon();

    const double& petitRayon() const;
    const double& grandRayon() const;
};

#endif /* ifndef __ELLIPSE_ */
