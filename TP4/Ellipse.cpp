#include "Ellipse.hpp"

//TODO finir exo7 figures

Ellipse::Ellipse(const double petit_rayon_a, const double grand_rayon_b)
  : m_petit_rayon_a(petit_rayon_a), m_grand_rayon_b(grand_rayon_b)
{}

double Ellipse::surface()
{
  return M_PI * m_petit_rayon_a * m_grand_rayon_b;
}

double& Ellipse::petitRayon()
{
  return m_petit_rayon_a;
}

double& Ellipse::grandRayon()
{
  return m_grand_rayon_b;
}

const double& Ellipse::petitRayon() const
{
  return m_petit_rayon_a;
}
const double& Ellipse::grandRayon() const
{
  return m_grand_rayon_b;
}
