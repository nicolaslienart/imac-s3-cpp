#include "Carre_heritage.hpp"

Carre::Carre(const double cote)
  : Rectangle::Rectangle(cote, cote)
{}

double& Carre::cote()
{
  return m_largeur;
}

const double& Carre::cote() const
{
  return m_largeur;
}

Carre::~Carre()
{
  std::cout << "Destruct de Carre" << std::endl;
}

void Carre::setCote(const double cote)
{
  m_largeur = cote;
  m_hauteur = cote;
}

void Carre::quiSuisJe() const
{
  std::cout << "Je suis un Carre" << std::endl;
}
