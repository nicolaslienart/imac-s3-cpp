#ifndef __CARRE_
#define __CARRE_

#include "Rectangle.hpp"

class Carre {
  private:
    double m_cote;
  public:
    Carre() = default;
    Carre(const double cote);
    ~Carre() =default;

    double& cote();
    const double& cote() const;

    double surface() const;
};

#endif /* ifndef __CARRE_ */

