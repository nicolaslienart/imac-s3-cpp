#include "Rectangle_heritage.hpp"

Rectangle::Rectangle(const double l, const double h)
  : m_largeur(l), m_hauteur(h)
{}

double& Rectangle::largeur()
{
  return m_largeur;
}

const double& Rectangle::largeur() const
{
  return m_largeur;
}

double& Rectangle::hauteur()
{
  return m_hauteur;
}

const double& Rectangle::hauteur() const
{
  return m_hauteur;
}

double Rectangle::surface() const
{
  return m_largeur * m_hauteur;
}

Rectangle::~Rectangle()
{
  std::cout << "Destruct de Rectangle" << std::endl;
}

void Rectangle::quiSuisJe() const
{
  std::cout << "Je suis un rectangle" << std::endl;
}
