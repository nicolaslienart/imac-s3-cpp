#ifndef __RECTANGLE_
#define __RECTANGLE_

#include <iostream>

class Rectangle {
  protected:
    double m_largeur = 0;
    double m_hauteur = 0;
  public:
    Rectangle() = default;
    Rectangle(const double l, const double h);
    ~Rectangle();

    double& largeur();
    const double& largeur() const;

    double& hauteur();
    const double& hauteur() const;

    double surface() const;

    void quiSuisJe() const;
};

#endif /* ifndef __RECTANGLE_ */
