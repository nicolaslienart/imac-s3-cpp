#include <iostream>
#include "Carre.hpp"

Carre::Carre(const double cote)
  : m_cote(cote)
{}

double& Carre::cote()
{
  std::cout << "version normale: " << std::endl;
  return m_cote;
}

const double& Carre::cote() const
{
  return m_cote;
}

double Carre::surface() const
{
  return m_cote * m_cote;
}
