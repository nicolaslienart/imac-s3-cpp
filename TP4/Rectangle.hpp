#ifndef __RECTANGLE_
#define __RECTANGLE_

class Rectangle {
  private:
    double m_largeur = 0;
    double m_hauteur = 0;
  public:
    Rectangle() = default;
    Rectangle(const double l, const double h);
    ~Rectangle() = default;

    double& largeur();
    const double& largeur() const;

    double& hauteur();
    const double& hauteur() const;

    double surface() const;
};

#endif /* ifndef __RECTANGLE_ */
