#ifndef __CARRE_
#define __CARRE_

#include <iostream>
#include "Rectangle_heritage.hpp"

class Carre : public Rectangle {
  public:
    Carre() = default;
    Carre(const double cote);
    ~Carre();

    double& largeur() = delete;
    double& hauteur() = delete;
    double& cote();
    void setCote(const double cote);
    const double& cote() const;
    void quiSuisJe() const;
};

#endif /* ifndef __CARRE_ */

